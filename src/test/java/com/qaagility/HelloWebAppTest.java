package com.qaagility;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

public class HelloWebAppTest extends Mockito {

	@Test
	public void testServlet() throws Exception {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);

		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		when(response.getWriter()).thenReturn(writer);

		new HelloWebApp().doGet(request, response);

		writer.flush(); // it may not have been flushed yet...
		System.out.print(stringWriter.toString());
		assertTrue("Expecting Hello WORLD but not found", stringWriter.toString().contains("Hello WORLD"));
	}

	@Test
	public void testAdd() throws Exception {

		int k = new HelloWebApp().add(8, 6);
		assertEquals("Problem with Add function:", 14, k);

	}

	@Test
	public void testSub() throws Exception {
		int k = new HelloWebApp().sub(8, 7);
		assertEquals("Problem with Sub function:", 1, k);

	}
	
	@Test
	public void testMul() throws Exception {
		int k = new HelloWebApp().mul(2, 2);
		assertEquals("Problem with Sub function:", 4, k);
	}

}
